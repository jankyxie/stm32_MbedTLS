
#ifndef _STM_HAL_MSP_H_
#define _STM_HAL_MSP_H_

void _Error_Handler(char *, int);
void HAL_MspInit(void);
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c);
void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c);
void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi);
void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi);
void HAL_UART_MspInit(UART_HandleTypeDef* huart);
void HAL_UART_MspDeInit(UART_HandleTypeDef* huart);
void HAL_PCD_MspInit(PCD_HandleTypeDef* hpcd);
void HAL_PCD_MspDeInit(PCD_HandleTypeDef* hpcd);

#endif


