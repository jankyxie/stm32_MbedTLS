/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice,
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other
  *    contributors to this software may be used to endorse or promote products
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under
  *    this license is void and will automatically terminate your rights under
  *    this license.
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "mbedtls.h"
#include "rng.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <time.h>
#include "string.h"
#include "mbedtls/pk.h"
#include "mbedtls/sha256.h"
#include "mbedtls/sha1.h"
#include "mbedtls/aes.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void mbedtls_aes_ecb_test(void);
void mbedtls_aes_cbc_test(void);
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	int i;
	unsigned char encrypt[] = "HeLLo ZhangShiSan!!";    
	unsigned char decrypt[32];
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_RNG_Init();
  MX_RTC_Init();
  MX_MBEDTLS_Init();
  /* USER CODE BEGIN 2 */
	
	// sha1  
	mbedtls_sha1_context sha1_ctx;
	mbedtls_sha1_init( &sha1_ctx );
	mbedtls_sha1_starts( &sha1_ctx );
	mbedtls_sha1_update( &sha1_ctx, encrypt, strlen((char *)encrypt) );
	mbedtls_sha1_finish( &sha1_ctx, decrypt);
	printf("\r\n原数据:%s \r\nsha1结果:", encrypt);
	mbedtls_sha1_free( &sha1_ctx );
	for (i = 0; i<20; i++)
	{
		printf("%02x", decrypt[i] );            
	}

	// sha256/224 
	mbedtls_sha256_context sha256_ctx;
	mbedtls_sha256_init( &sha256_ctx );
	mbedtls_sha256_starts( &sha256_ctx,0 ); // 0 - sha256 1 - sha224
	mbedtls_sha256_update( &sha256_ctx, encrypt, strlen((char *)encrypt) );
	mbedtls_sha256_finish( &sha256_ctx, decrypt);
	printf("\r\n原数据:%s\r\nsha256结果:", encrypt);
	mbedtls_sha256_free( &sha256_ctx );
	
	for (i = 0; i<32; i++)
	{
		printf("%02x", decrypt[i] );            
	}
	printf("\r\n");
	
	// AES ecb 16字节加密
	mbedtls_aes_ecb_test();
	
	// AES cbc 16字节整数倍加密
  mbedtls_aes_cbc_test();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1)
    {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /**Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

int fputc( int dat, FILE *file )
{
    uint8_t ch = dat;
    HAL_UART_Transmit(&huart1,&ch,1,100 );
		return 1;
}


_ARMABI time_t time(time_t *t)
{
	RTC_DateTypeDef sDate;
	RTC_TimeTypeDef sTime;
	struct tm t1;
	time_t it;

	HAL_RTC_GetDate(&hrtc,&sDate,RTC_FORMAT_BIN);
	HAL_RTC_GetTime(&hrtc,&sTime,RTC_FORMAT_BIN);

	if (t) return *t;
	else if ( 1 )
	{
		t1.tm_year = sDate.Year + 100;
		t1.tm_mon  = sDate.Month - 1;
		t1.tm_mday = sDate.Date;
		t1.tm_hour = sTime.Hours;
		t1.tm_min  = sTime.Minutes;
		t1.tm_sec  = sTime.Seconds;

		it = mktime(&t1);
		return it ;
	}
	
	return -1;
}


void mbedtls_aes_ecb_test(void)
{
	int i;
	mbedtls_aes_context aes_ctx;
	
	//密钥数值
	unsigned char key[16] = "ECBPASSWD1234";
 
	//明文空间
	unsigned char plain[16] = "ZhangShiSan!!";
	//解密后明文的空间
	unsigned char dec_plain[16]={0};
	//密文空间
	unsigned char cipher[16]={0};
 
	mbedtls_aes_init( &aes_ctx );
	
	//设置加密密钥
	mbedtls_aes_setkey_enc( &aes_ctx, key, 128);
	printf("\r\nAES ECB:\r\n加密前: %s\r\n", plain);
	mbedtls_aes_crypt_ecb( &aes_ctx, MBEDTLS_AES_ENCRYPT, plain, cipher);
	
	printf("加密后: ");
	for (i = 0; i<16; i++)
	{
		printf("%c", cipher[i] );            
	}
	printf(" -End\r\n");
	   
	//设置解密密钥
	mbedtls_aes_setkey_dec(&aes_ctx, key, 128);
	mbedtls_aes_crypt_ecb( &aes_ctx, MBEDTLS_AES_DECRYPT, cipher, dec_plain );
	printf("解密后: %s\r\n", dec_plain);
	mbedtls_aes_free( &aes_ctx );
}

void mbedtls_aes_cbc_test(void)
{
	int i;
	
	mbedtls_aes_context aes_ctx;
	
	//密钥数值
	unsigned char key[16] = "CBCPASSWD1234";
	//iv
	unsigned char iv[16];
	
	//明文空间
	unsigned char plain[64] = "ZhangShiSan!!ZhangShiSan!!ZhangShiSan!!ZhangShiSan!!";
	//解密后明文的空间
	unsigned char dec_plain[64]={0};
	//密文空间
	unsigned char cipher[64]={0};
 
	
	mbedtls_aes_init( &aes_ctx );
	
	//设置加密密钥
	printf("\r\nAES CBC:\r\n加密前: %s\r\n", plain);
	mbedtls_aes_setkey_enc( &aes_ctx, key, 128);
	for(i = 0; i < 16; i++)
	{
		iv[i] = 0x01;
	}
	mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_ENCRYPT, 64, iv, plain, cipher);
	printf("加密后: ");
	for (i = 0; i<64; i++)
	{
		printf("%c", cipher[i] );            
	}
	printf(" -End\r\n");
	   
	//设置解密密钥
	mbedtls_aes_setkey_dec(&aes_ctx, key, 128);
	for(i = 0; i < 16; i++)
	{
		iv[i] = 0x01;
	}
	mbedtls_aes_crypt_cbc(&aes_ctx, MBEDTLS_AES_DECRYPT, 64, iv, cipher, dec_plain);
	printf("解密后: %s\r\n", dec_plain);
	printf("\r\n");
	mbedtls_aes_free( &aes_ctx );
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
       tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
